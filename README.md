# edouard

E.D.O.U.A.R.D. stands for 
**E**ffective
**D**evops
**O**piniated
**U**p
**A**nd
**R**unning
**D**eck.

This is a set of tools 

* **E**ffective : this is not a tutorial or demo, those tools are meant to be used
* **D**evops : we are not full sysadmin ; we are not full developper ; we need tools for this new blurred devops space
* **O**piniated : this is our stack ; those are our choices ; we may be wrong ; we will frequently change our mind
* **U**p **A**nd **R**unning : Whatever the task, the tools should help to set it up and run it
* **D**eck : Why is there a **D** at this end of this forename ?

## Available tools

* [k8scli](/k8scli/index.md) : set up all the cli tools needed for a kubernetes workstation
* [microk8s](/microk8s/index.md) : set up a microk8s kubernetes distribution
* [devstation](/devstation/index.md) : basic dev workstation deployment
* [fakeapps](/fakeapps/index.md) : basic app to test deployment
* [keepass2secrets](/keepass2secrets/README.md) : basic app to convert Keepass file to secrets.yaml
* [devstation](/devstation/index.md) : set up all the tools for a development workstation


