# devstation

Set up all the tools for a development workstation

## pre-requiesites

based on Ubuntu LTS 22.04 desktop AMD64 with ssh

```shell
mkdir ~/src

sudo apt install --yes \
  byobu \
  curl \
  git \
  pipx \
  python3-dev \
  rclone \
  rcm \
  ssh \
  terminator \
  wireguard

sudo snap install --classic code

sudo snap install keepassxc


# pyenv
curl https://pyenv.run | bash
# pyenv deps, see https://github.com/pyenv/pyenv/wiki#suggested-build-environment)
sudo apt install build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev curl \
libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

# git
git config --global user.email "philippe@arundo.tech"
git config --global user.name "Philippe ENTZMANN"


# peotry
pipx install poetry
# logout/login to activate


# rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
# logout/login to activate

# nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
# logout/login to activate

# node LTS (18)
nvm install --lts
nvm use --lts
# enable yarn and pnpm
corepack enable
# yarn
#npm install --global yarn
# pnpm
#curl -fsSL https://get.pnpm.io/install.sh | sh -

# podman
sudo apt install --yes podman podman-docker docker-compose
# in your ~./bashrc file add docker as a podman alias (alias docker=podman)
# add to the end of /etc/containers/registries.conf :
# unqualified-search-registries = ['docker.io']
# 
# [[registry]]
# prefix = "docker.io"
# location = "docker.io"
# 
# [[registry.mirror]]
# prefix = "docker.io"
# location = "docker.mirrors.ustc.edu.cn"

# starship
curl -sS https://starship.rs/install.sh | sh
# sudo, confirm, add to .bashrc

# helix 
sudo add-apt-repository ppa:maveonair/helix-editor
sudo apt update
sudo apt install helix

# via package manager
# * nextcloud client 
# * inkscape (install last PPA)
# * signal
```

## install via ansible (OBSOLETE)

From any other workstation (including the target one) :
```
#sudo apt install software-properties-common
#sudo apt-add-repository ppa:ansible/ansible
sudo apt update
sudo apt install ansible

ansible-galaxy collection install community.general
```


```shell
# -K to ask for sudo password
ansible-playbook devtools.yaml -K --limit=192.168.200.111
```


## TODO

add :
* shotcut
* glaxnimate
* mattermost
* helix
* restore dotfiles
* obs
