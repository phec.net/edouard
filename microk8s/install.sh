# Remove the previous installation
sudo snap remove --purge microk8s

# install microck8s
#sudo snap install microk8s --classic --channel=1.18/stable
sudo snap install microk8s --classic --channel=1.20/stable

# set user access
USER=`whoami`
sudo usermod -a -G microk8s ${USER}
sudo chown -f -R ${USER} ~/.kube
mkdir -p ~/.kube

# start microk8s
sudo microk8s.start
sudo microk8s.config > ~/.kube/config
sudo microk8s.enable dns registry storage
#sudo microk8s.enable dns helm registry storage
#sudo microk8s.helm init
#sudo microk8s.helm repo update

#sudo wget -nv https://get.helm.sh/helm-v2.17.0-linux-amd64.tar.gz
#sudo tar -xf helm-*.tar.gz
#sudo mv linux-amd64/helm /usr/local/bin/helm
sudo snap install helm --classic
sudo helm init --stable-repo-url=https://charts.helm.sh/stable --upgrade
sudo helm repo update

# wait for  services ...
kubectl -n kube-system wait deployment.apps/coredns --for=condition=available --timeout=-1s
kubectl -n container-registry wait deployment.apps/registry --for=condition=available --timeout=-1s
kubectl -n kube-system wait deployment.apps/tiller-deploy --for=condition=available --timeout=-1s


echo "✅ microk8s installed"
echo "✅ addons installed : dns, helm, registry, storage"
echo "✅ ~/.kube/config file set"

