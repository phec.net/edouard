# Install ingress controller
microk8s.kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-0.32.0/deploy/static/provider/baremetal/deploy.yaml
#microk8s.kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.35.0/deploy/static/provider/baremetal/deploy.yaml
# Apply hostnetwork patch
microk8s.kubectl patch deployment ingress-nginx-controller \
    -n ingress-nginx \
    --type merge \
    --patch '{"spec": {"template": {"spec": {"hostNetwork": true}}}}'
# Install cert-manager
#microk8s.kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.15.0/cert-manager.yaml
microk8s.kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.0.1/cert-manager.yaml
# wait for deployments ...
microk8s.kubectl -n cert-manager wait deployment.apps/cert-manager --for=condition=available --timeout=-1s
microk8s.kubectl -n cert-manager wait deployment.apps/cert-manager-cainjector --for=condition=available --timeout=-1s
microk8s.kubectl -n cert-manager wait deployment.apps/cert-manager-webhook --for=condition=available --timeout=-1s
microk8s.kubectl -n ingress-nginx wait deployment.apps/ingress-nginx-controller --for=condition=available --timeout=-1s
echo "✅ ingress controller with hostnetwork patch installed"
echo "✅ cert-manager installed"