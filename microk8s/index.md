# microk8s

Set up a microk8s kubernetes distribution.



## Install microk8s and addons

```bash
bash <(curl -s https://gitlab.com/phec.net/edouard/-/raw/master/microk8s/install.sh)
```

## Install ingress and cert-manager

```bash
bash <(curl -s https://gitlab.com/phec.net/edouard/-/raw/master/microk8s/ingress-cert-manager.sh)
```

We install a basic ingress controller that need to be patched since we don't have any loadbalancer and want our ingress controller to have a direct host network access.

Here is the YAML patch to apply :
```yaml
spec:
  template:
    spec:
      hostNetwork: true
```

It is applied with the following part of the script :
```bash
kubectl patch deployment ingress-nginx-controller \
    -n ingress-nginx \
    --type merge \
    --patch '{"spec": {"template": {"spec": {"hostNetwork": true}}}}'
```    