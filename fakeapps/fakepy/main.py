import json
import subprocess
import time
from pathlib import Path

from fastapi import BackgroundTasks, FastAPI, HTTPException
from fastapi.responses import Response, HTMLResponse, JSONResponse
from prometheus_client import Counter
from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request
from starlette_exporter import PrometheusMiddleware, handle_metrics


app = FastAPI(    
    title="fakepy",
    description="ADM Pets API",
    version="1.0.0",
    openapi_url="/api/openapi.json",
    docs_url="/api/docs",
    redoc_url="/api/redoc",
)
# CORS
origins = [
    "http:localhost",
    "http:localhost:8080",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.add_middleware(PrometheusMiddleware,
    app_name='fakepy', prefix='mymetrics_')
app.add_route("/api/metrics", handle_metrics)


REDIRECT_COUNT = Counter("redirect_total", "Count of redirects", ("from",))
ERIC_COUNT = Counter("eric_count", "Count of Eric'a API call")

@app.get("/",  response_class=HTMLResponse)
async def get_root():
    return '''
        <h1>fakepy API app</h1>
        <ul>
            <li><a href="/api/metrics">metrics</a></li>
            <li><a href="/api/eric">/api/eric</a></li>
            <li><a href="/api/toto">/api/toto</a></li>
            <li><a href="/api/docs">/api/docs</a></li>
            <li><a href="/api/redoc">/api/redoc</a></li>
        </ul>
    '''


@app.get("/api/eric")
def get_eric():
    '''
    Première **API** pour Eric
    '''
    d = { 'hello' : 'world', 'y' : time.time()}
    ERIC_COUNT.inc()
    return d

@app.get("/api/toto")
async def get_toto():
    REDIRECT_COUNT.labels("some_view").inc()
    return "otot"

# = == === O === == - = == === O === == - = == === O === == - = == === O === = #

@app.exception_handler(NotImplementedError)
@app.exception_handler(Exception)
async def exception_handler(request: Request, exc: Exception):
    name = exc.name if hasattr(exc, "name") else exc.__class__.__name__
    message = str(exc)
    print(f"!!! exception_handler {message}")
    return JSONResponse(status_code=500, content={"message": message, "name": name})
