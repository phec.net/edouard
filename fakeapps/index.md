# fakeapp

fake app with its helm chart.

## install from release

```bash
# deploy blue app on default domain  
helm install \
    --name fakeappblue \
    --namespace fakeappblue \
    https://gitlab.com/phec.net/edouard/-/jobs/562828771/artifacts/raw/fakeapp-0.1.0.tgz
```


## install from local repo

Sample deployment commands :

```bash
# deploy blue app on default domain  
helm install \
    --name fakeappblue \
    --namespace fakeappblue \
    ./chart/fakeapp
```

```bash
# deploy purple app on alternate domain  
helm install \
    --name fakeapppurple \
    --namespace fakeapppurple \
    -f ./chart/fakeapp/values_override_purple.yaml \
    ./chart/fakeapp
```    


```bash
# deploy both app with let's encrypt prod certificates
# BEWARE OF THE RATE LIMIT !!!!
helm install \
    --name fakeappblue \
    --namespace fakeappblue \
    -f ./chart/fakeapp/values_override_letsencrypt_prod.yaml \
    ./chart/fakeapp
helm install \
    --name fakeapppurple \
    --namespace fakeappblue \
    -f ./chart/fakeapp/values_override_purple.yaml \
    -f ./chart/fakeapp/values_override_letsencrypt_prod.yaml \
    ./chart/fakeapp
```    

