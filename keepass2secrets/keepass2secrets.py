#!/usr/bin/python

import base64
import click
import datetime
from pathlib import Path
from pykeepass import PyKeePass
from pykeepass.exceptions import CredentialsError
import sys


@click.command()
@click.option('--key', default=None, help='Fichier clé (.key)')
@click.argument('keepass_path')
@click.option('--password', prompt='Mot de passe', hide_input=True, help='KDBX Password')
def get_secrets(keepass_path, password, key=None):
    print(f"# Généré le {datetime.datetime.now().strftime('%d/%m/%Y à %H:%M')} à partir du fichier Keepass : {Path(keepass_path).name}")
    print('# ----------------------------------------')

    try:
        if key is None:
            kp = PyKeePass(keepass_path, password=password)
        else:
            kp = PyKeePass(keepass_path, password=password, keyfile=key)
    except CredentialsError:
        print("ERREUR : Le mot de passe n'est pas le bon ou le fichier clé n'est pas correct")
        sys.exit(0)
    except FileNotFoundError:
        print("ERREUR : Le fichier indiqué n'a pas été trouvé. Assurez-vous qu'il soit disponible dans le volume monté")
        sys.exit(0)
        
    print("apiVersion: v1")
    print("kind: Secret")
    print("metadata:")
    print(f"  name: {Path(keepass_path).stem}")
    print("type: Opaque")
    print("data:")
    for x in kp.entries:
        print(f"  {x.username}: {base64.b64encode(x.password.encode('utf8')).decode('utf8')}")

    print('# ----------------------------------------')

if __name__ == "__main__":
    print("#  _                                 ____                         _")       
    print("# | | _____  ___ _ __   __ _ ___ ___|___ \ ___  ___  ___ _ __ ___| |_ ___ ")
    print("# | |/ / _ \/ _ \ '_ \ / _` / __/ __| __) / __|/ _ \/ __| '__/ _ \ __/ __|")
    print("# |   <  __/  __/ |_) | (_| \__ \__ \/ __/\__ \  __/ (__| | |  __/ |_\__ \ ")
    print("# |_|\_\___|\___| .__/ \__,_|___/___/_____|___/\___|\___|_|  \___|\__|___/")
    print("#               |_|")

    get_secrets()