# Outil pour générer un fichier Secret à partir d'une base KeePass

Voici deux exemples de fichiers KeePass

* SITE_PROD_key.kdbx : monmotdepasse
* SITE_PROD_key.kdbx : MONMOTDEPASSE + key


## Utilisation Docker

Placez vous dans le dossier où se trouve votre fichier KeePass.

```bash
docker run --rm -it \
    -v $(pwd):/data:ro \
    registry.gitlab.com/phec.net/edouard/keepass2secrets:latest \
    ./SITE_PROD.kdbx

docker run --rm -it \
    -v $(pwd):/data:ro \
    registry.gitlab.com/phec.net/edouard/keepass2secrets:latest \
    ./SITE_PROD.kdbx --password monmotdepasse

docker run --rm -it \
    -v $(pwd):/data:ro \
    registry.gitlab.com/phec.net/edouard/keepass2secrets:latest \
    ./SITE_PROD_key.kdbx --key ./SITE_PROD.key

docker run --rm -it \
    -v $(pwd):/data:ro \
    registry.gitlab.com/phec.net/edouard/keepass2secrets:latest \
    ./SITE_PROD_key.kdbx --key ./SITE_PROD.key --password MONMOTDEPASSE > secrets.yaml
```

### Construction du docker

```bash
docker build -t registry.gitlab.com/phec.net/edouard/keepass2secrets:latest .

docker push registry.gitlab.com/phec.net/edouard/keepass2secrets:latest
```


## Utilisation Poetry

```bash
poetry run python keepass2secrets.py ./SITE_PROD.kdbx

poetry run python keepass2secrets.py ./SITE_PROD.kdbx --password monmotdepasse

poetry run python keepass2secrets.py ./SITE_PROD_key.kdbx --key ./SITE_PROD.key

poetry run python keepass2secrets.py ./SITE_PROD_key.kdbx --key ./SITE_PROD.key --password MONMOTDEPASSE
```

### Construction de l'environnement

```bash
pyenv sheel 3.8.2

poetry install
```