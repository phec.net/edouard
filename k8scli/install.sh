#!/bin/bash

DOCKER_VERSION=5:19.03.9~3-0~ubuntu-focal
# cf https://nodejs.org/en/
NODE_VERSION=16
# cf https://www.python.org/downloads/
#PYTHON_VERSION=3.9.7
PYTHON_VERSION=3.8.10
# cf https://github.com/caddyserver/caddy/releases
#CADDY_VERSION=v1.0.4
CADDY_VERSION=2.5.1
# cf https://github.com/kubernetes/kubernetes/releases
K8S_VERSION=v1.22.11
# cf https://github.com/helm/helm/releases
#HELM_VERSION=v2.16.10
# cf https://github.com/istio/istio/releases/
#ISTIO_VERSION=1.9.0
# cf https://github.com/GoogleContainerTools/skaffold/releases
SKAFFOLD_VERSION=v1.38.0
# cf https://github.com/derailed/k9s/releases
K9S_VERSION=v0.25.18
# cf https://github.com/minio/minio/releases
# cf https://dl.min.io/server/minio/release/linux-amd64/
#MINIO_VERSION=RELEASE.2021-10-10T16-53-30Z

# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .

sudo apt-get update
sudo apt-get install -y \
    apt-transport-https \
    build-essential \
    byobu \
    ca-certificates \
    containerd \
    curl \
    docker.io \
    git \
    gnupg-agent \
    libbz2-dev \
    libffi-dev \
    liblzma-dev \
    libncurses5-dev \
    libncursesw5-dev \
    libreadline-dev \
    libsqlite3-dev \
    libssl-dev \
    llvm \
    make \
    python-openssl \
    python3 \
    python3-pip \
    rclone \
    software-properties-common \
    tk-dev \
    tmuxp \
    wget \
    xz-utils \
    zlib1g-dev


# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# docker (OBSOLETE, installed via apt)
#sudo apt-get remove --yes \
#    docker docker-engine docker.io containerd runc
#curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
#sudo add-apt-repository \
#   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
#   $(lsb_release -cs) \
#   stable"
#sudo apt-get update
#sudo apt-get install --yes \
#    docker-ce=$DOCKER_VERSION \
#    docker-ce-cli=$DOCKER_VERSION \
#    containerd.io
# add current user as docker operator
sudo usermod -aG docker `whoami`


# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# pyenv
sudo rm -R ~/.pyenv
curl https://pyenv.run | bash

# cf https://github.com/pyenv/pyenv
# the sed invocation inserts the lines at the start of the file
# after any initial comment lines
sed -Ei -e '/^([^#]|$)/ {a \
export PYENV_ROOT="$HOME/.pyenv"
a \
export PATH="$PYENV_ROOT/bin:$PATH"
a \
' -e ':a' -e '$!{n;ba};}' ~/.profile
echo 'eval "$(pyenv init --path)"' >>~/.profile
echo 'eval "$(pyenv init -)"' >> ~/.bashrc

# restart session or ...
# export PYENV_ROOT="$HOME/.pyenv"
# export PATH="$PYENV_ROOT/bin:$PATH"
# eval "$(pyenv init --path)"
# eval "$(pyenv init -)"

# reload profile
source ~/.profile

# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# python
pyenv install $PYTHON_VERSION
pyenv global $PYTHON_VERSION
pyenv local $PYTHON_VERSION

# pip
wget https://bootstrap.pypa.io/ez_setup.py -O - | sudo python
~/.pyenv/versions/$PYTHON_VERSION/bin/pip install -U virtualenv

#sudo update-alternatives \
#    --install /usr/bin/python python ~/.pyenv/versions/$PYTHON_VERSION/bin/python 10
#
#sudo update-alternatives \
#    --install /usr/bin/python3 python3 ~/.pyenv/versions/$PYTHON_VERSION/bin/python 10

# python deps
#TODO: move to poetry
sudo ~/.pyenv/versions/$PYTHON_VERSION/bin/pip install \
    python-dotenv \
    typer
#awscli \
#awscli-plugin-endpoint     \
#pytest==4.4.* \
#python-openstackclient==3.8.* \
#python-cinderclient==1.11.* \
#python-designateclient \
#python-glanceclient==2.6.* \
#python-keystoneclient==3.10.* \
#python-neutronclient==6.1.* \
#python-novaclient==7.1.* \
#oslo.log \
#python-heatclient \
#python-ironicclient \
#requests==2.22.* \
#shade


# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# poetry
#curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
curl -sSL https://install.python-poetry.org | python3 -
source $HOME/.poetry/env
poetry config virtualenvs.in-project true


# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# gitlab runner
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod a+x /usr/local/bin/gitlab-runner


# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# nodejs install
curl -sL https://deb.nodesource.com/setup_$NODE_VERSION.x | sudo -E bash -
sudo apt-get install -y nodejs


# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# Yarn install
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt install --yes yarn


# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# vue cli
sudo yarn global add @vue/cli


# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# kubectl install
curl -LO https://storage.googleapis.com/kubernetes-release/release/$K8S_VERSION/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl


# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# skaffold
#curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64
curl -Lo skaffold https://github.com/GoogleContainerTools/skaffold/releases/download/$SKAFFOLD_VERSION/skaffold-linux-amd64
chmod +x skaffold
sudo mv skaffold /usr/local/bin


# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# k9s : https://github.com/derailed/k9s
wget https://github.com/derailed/k9s/releases/download/$K9S_VERSION/k9s_Linux_x86_64.tar.gz
tar xf k9s_Linux_x86_64.tar.gz
sudo mv k9s /usr/local/bin
rm k9s_Linux_x86_64.tar.gz LICENSE README.md

# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# Helm install
#wget https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz
#tar -zxvf helm-*.tar.gz
#sudo mv linux-amd64/helm /usr/local/bin/helm
#rm helm-*.tar.gz*
#sudo rm -R linux-amd64
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
rm get_helm.sh


# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# caddy 1 (OBSOLETE, use caddy 2)
# wget https://github.com/caddyserver/caddy/releases/download/v1.0.4/caddy_v1.0.4_linux_amd64.tar.gz
# wget https://github.com/caddyserver/caddy/releases/download/v1.0.4/caddy_v1.0.4_linux_amd64.tar.gz
#pushd /tmp
#wget https://github.com/caddyserver/caddy/releases/download/${CADDY_VERSION}/caddy_${CADDY_VERSION}_linux_amd64.tar.gz
#tar xf caddy_${CADDY_VERSION}_linux_amd64.tar.gz
#sudo mv caddy /usr/local/bin/
#popd

# caddy2
# wget https://github.com/caddyserver/caddy/releases/download/v2.4.1/caddy_2.4.1_linux_amd64.tar.gz
pushd /tmp
wget https://github.com/caddyserver/caddy/releases/download/v${CADDY_VERSION}/caddy_${CADDY_VERSION}_linux_amd64.tar.gz
tar xf caddy_${CADDY_VERSION}_linux_amd64.tar.gz
sudo mv caddy /usr/local/bin/caddy
popd


# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# minio
# Fetch the latest (or the link will broke weekly !!!)
MINIO_FILENAME=`wget -q -O - https://dl.min.io/server/minio/release/linux-amd64/ | grep -Eo 'minio_[^"]*_amd64\.deb' | sort -u` \
wget --quiet https://dl.min.io/server/minio/release/linux-amd64/$MINIO_FILENAME \
dpkg -i minio_*_amd64.deb \
rm *.deb



# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# telepresence
curl -s https://packagecloud.io/install/repositories/datawireio/telepresence/script.deb.sh | sudo bash
sudo apt install --yes --no-install-recommends telepresence

# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
# keep sorted alphabeticaly
echo "✅ caddy $CADDY_VERSION"
echo "✅ docker $DOCKER_VERSION"
echo "✅ gitlab-runner"
echo "✅ helm3"
echo "✅ k9s $K9S_VERSION"
echo "✅ kubectl $K8S_VERSION"
echo "✅ minio $MINIO_FILENAME"
echo "✅ nodejs $NODE_VERSION"
echo "✅ python/pyenv/poetry $PYTHON_VERSION"
echo "✅ skaffold $SKAFFOLD_VERSION"
echo "✅ some python packages"
echo "✅ some ubuntu packages"
echo "✅ telepresence"
echo "✅ vuecli"
echo "✅ yarn"

# . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. ... .. . .. .
