# k8scli

Set up all the tools needed for a kubernetes workstation.

Install `curl` :
```
sudo apt install curl
```

To install the last version :
```bash
bash <(curl -s https://gitlab.com/phec.net/edouard/-/raw/master/k8scli/install.sh)
```

To install a given version :
```bash
bash <(curl -s https://gitlab.com/phec.net/edouard/-/raw/v0.4.4/k8scli/install.sh)
```

This script installs :

* **other tools to be listed ...**
* caddy 1.0.4
* caddy 2.4.1
* docker 19.03.9
* gitlab runner
* helm3
* istio 1.5.0 (cli)
* k9s 0.24.2
* kubectl 1.21.1 (cli)
* minio (cli)
* nodejs 14.x
* python 3.9.1 (via pyenv)
* python poetry
* python pyenv
* skaffold 1.25.0
* telepresence
* byobu / tmux / tmuxp
* vuecli (cli)
* yarn

